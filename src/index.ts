import {
    UniversityTeacher, UniversityStudent, UniversityLibrary, UniversityFaculty,
    UniversitySubject, UniversityCanteen, FacultyStudentTicketHandler, University,
    InternshipManager, Internship, Practice, Rector
} from './classes/class';
import { ScheduleforSport, WeekDay } from './enums/enums';
import {
    StudentTicket, MyStudent, StudentIdName, Announcement, OpenDayVisitor,
    isApplication, Application
} from './types/type';
import { Section } from './interfaces/interface';
import { RectorDecorator } from './decorators/decorator';

// Testing our implementation

//---------------Title------------

// Creation of the name of the university and output of its name
const universityName = "ТНТУ ім. Івана Пулюя";

console.log('---');
console.log(`University: ${universityName}`);
console.log('---');

//---------------------Actions by rector-------------
const rector = new Rector();
rector.greet('Менторе');
const decoratedRector = new (RectorDecorator(Rector))();
decoratedRector.welcome();



console.log('---');
console.log('Дії ректора сьогодні:\n')

const application: Application = {
  name: 'Василь',
  department: 'Математика',
  title: 'Заява на премію',
  description: 'Заява на участь у конкурсі на премію у галузі математики.',
};
rector.signApplication(application);

const announcement: Announcement = {
  title: 'Новий курс з програмування',
  description: 'Запрошуємо всіх студентів на новий курс з програмування!',
};
rector.makeAnnouncement(announcement);

const doc: Application = {
  name: 'Петро',
  department: 'Історія',
  title: 'Додатковий матеріал для історії',
  description: 'Надається додатковий матеріал для студентів зі спеціальності "Історія".',
};
if (isApplication(doc)) {
  rector.signApplication(doc);
}

const visitor: OpenDayVisitor = {
  name: 'Марія',
  email: 'maria@example.com',
};
rector.conductOpenDay(visitor);

console.log('---');
// Create faculties
const faculties: UniversityFaculty[] = [
  new UniversityFaculty('Faculty of Science', 'Dr. Johnson', 100),
  new UniversityFaculty('Faculty of Arts', 'Dr. Smith', 80),
];

// Create subjects
const subjects: UniversitySubject[] = [
  new UniversitySubject('Math', 'Room A101', 3),
  new UniversitySubject('Physics', 'Room B202', 2),
  new UniversitySubject('Chemistry', 'Room 303', 3),
  new UniversitySubject('Biology', 'Room 404', 2)
];

// Create teachers
const teachers: UniversityTeacher[] = [
  new UniversityTeacher('John Doe', 100, [subjects[0], subjects[1]], [ WeekDay.Monday,  WeekDay.Wednesday], true),
  new UniversityTeacher('Jane Smith', 90, [subjects[2], subjects[3]], [WeekDay.Tuesday, WeekDay.Thursday], true),
];

// Create students
const students: UniversityStudent[] = [
  new UniversityStudent('Alice Johnson', true, [4, 3, 5], 2, 100),
  new UniversityStudent('Bob Smith', false, [5, 1, 2], 3),
];

// Create canteen
const canteenMenu: UniversityCanteen[] = [
  new UniversityCanteen(WeekDay.Monday, ['Soup', 500, 'g'], 5),
  new UniversityCanteen(WeekDay.Tuesday, ['Salad', 230, 'g'], 6),
  new UniversityCanteen(WeekDay.Wednesday, ['Soup', 480, 'g'], 5),
  new UniversityCanteen(WeekDay.Thursday, ['Salad', 190, 'g'], 6),
  new UniversityCanteen(WeekDay.Friday, ['Soup', 360, 'g'], 5),
];

// Create librarySchedule
const librarySchedule: UniversityLibrary[] = [
  new UniversityLibrary(WeekDay.Monday, '9:00 AM', '5:00 PM'),
  new UniversityLibrary(WeekDay.Tuesday, '9:00 AM', '5:00 PM'),
  new UniversityLibrary(WeekDay.Wednesday, '9:00 AM', '5:00 PM'),
  new UniversityLibrary(WeekDay.Thursday, '9:00 AM', '8:00 PM'),
  new UniversityLibrary(WeekDay.Friday, '9:00 AM', '4:00 PM'),
];

//---------------Students tickets------------------------
const ticketHandler = new FacultyStudentTicketHandler("Faculty of University");

// Function for generate id for student
const getId = (() => {
  let counter = 0;
  return () => counter++;
})();

// Create students tickets
const ticket1: StudentTicket = {
  id: getId(),
  name: students[0].name,
  faculty: faculties[0].name,
  course: students[0].course,
};

const ticket2: StudentTicket = {
  id: getId(),
  name: students[1].name,
  faculty: faculties[1].name,
  course: students[1].course,
};

// Add student tickets

ticketHandler.addTicket(ticket1);
ticketHandler.addTicket(ticket2);

//---------------Internships and practices----------------------------

// Creating an internship and practice manager
const manager = new InternshipManager<StudentIdName>();


// Creating instances of internship and internship opportunities
const internshipOpportunity = new Internship<StudentIdName>('Company ITspecialst');
const practiceOpportunity = new Practice<StudentIdName>('Company Bussiness');

// Adding capabilities to the manager
manager.addOpportunity(internshipOpportunity);
manager.addOpportunity(practiceOpportunity);

//------------------------Output---------------------------------------------//

// Output of the list of faculties with their deans and the number of state seats
console.log('Список факультетів:\n');
faculties.forEach((faculty) => {
  console.log(`Факультет: ${faculty.name}`);
  console.log(`Декан: ${faculty.dean}`);
  console.log(`Кількість державних місць: ${faculty.governmentSeats}`);
  console.log('---');
});

// Output of the list of subjects with the audience and the number of modules
console.log('Список предметів:\n');
subjects.forEach((subject) => {
  console.log(`Предмет: ${subject.name}`);
  console.log(`Аудиторія: ${subject.classroom}`);
  console.log(`Кількість модулів: ${subject.moduleCount}`);
    console.log('---');
    
});

// Output of the list of teachers with their salaries and work schedule
console.log('Список викладачів:\n');
teachers.forEach((teacher) => {
  console.log(`Ім'я: ${teacher.name}`);
  console.log(`Зарплата: $${teacher.calculateSalary()}`);
  console.log(`Викладає предмети: ${teacher.subjects.map(subject => subject.name).join(', ')}`);
  console.log(`Робочі дні: ${teacher.workingDays.join(', ')}`);
  console.log('---');
});

teachers.forEach((teacher) => {
  teacher.attendSeminar("Advanced Mathematics");
});
 console.log('---');

// List of students with their scholarship (if any) and average grade
console.log('Список студентів:\n');
students.forEach((student) => {
  console.log(`Ім'я: ${student.name}`);
  console.log(`Середній бал: ${student.averageGrade()}`);
  console.log(`Курс: ${student.course}`);
  if (student.isGovernmentForm) {
    console.log(`Стипендія: $${student.scholarship}`);
  }
  console.log('---');
});

// Placing a student on internship or practice opportunities
manager.placeStudents(students);
console.log('---')

// Output of the schedule of the library for each day of the week
console.log('Графік роботи бібліотеки:\n');
librarySchedule.forEach((schedule) => {
  console.log(`День: ${schedule.day}`);
  console.log(`Відкривається о: ${schedule.openingTime}`);
  console.log(`Закривається о: ${schedule.closingTime}`);
  console.log('---');
});

// Output of the canteen menu for each day of the week and the prices of dishes
console.log('Меню у їдальні:\n');
canteenMenu.forEach((menu) => {
  console.log(`День: ${menu.day}`);
  console.log(`Меню: ${menu.menu[0]}, ${menu.menu[1]}${menu.menu[2]}`);
  console.log(`Ціна: $${menu.price}`);
  console.log('---');
});

// Withdrawal of student tickets

console.log("\n--- Студентські квитки ---");
console.log(`\nКількість студентських квитків: ${ticketHandler.getTicketCount()}\n`);
ticketHandler.tickets.forEach(ticket => {
  console.log(ticketHandler.getTicketInfo(ticket));
  console.log('\n');
});



// -----------------------Sports sections---------------
// We create an instance of the university and add several sports sections

const university = new University();

// Function for generate id for section
const getIdSection = (() => {
  let counter = 1000;
  return () => counter++;
})();

const footballSection: Section = {
  id: getIdSection(),
  name: 'Футбол',
  schedule: ScheduleforSport.Monday,
  capacity: 15,
  trainer: 'Іванов Іван',
  studentsSection: [],
};

const basketballSection: Section = {
  id: getIdSection(),
  name: 'Баскетбол',
  schedule: ScheduleforSport.Wednesday,
  capacity: 12,
  trainer: 'Петров Петро',
  studentsSection: [],
};

university.sections=[footballSection, basketballSection];

// Enrollment and withdrawal of students

const student1: MyStudent = {name: students[0].name, course: students[0].course};
const student2: MyStudent = {name: students[1].name, course: students[1].course};

console.log('\n')

university.enrollStudent(student1, 1000); // Записати Олега на секцію футболу
university.enrollStudent(student2, 1000); // Записати Марію на секцію футболу
university.withdrawStudent(student1, 1000); // Виписати Олега з секції футболу

console.log('\n')
// Output of the list of sections

console.log(JSON.stringify(university.sections, null, 2));