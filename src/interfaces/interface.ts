import { WeekDay, ScheduleforSport } from "../enums/enums";
import { MyStudent } from "../types/type";
import { UniversitySubject } from "../classes/class";

// Interface for canteen
export interface Canteen {
  day: WeekDay;
  menu: [string, number, string];
  price: number;
}

// Interface for Faculty
export interface Faculty {
  name: string;
  dean: string;
  governmentSeats: number;
}

// Interface for library
export interface Library {
  day: WeekDay;
  openingTime: string;
  closingTime: string;
}

// Interface for sport section
export interface Section {
  id: number;
  name: string;
  schedule: ScheduleforSport;
  capacity: number;
  trainer: string;
  studentsSection: MyStudent[];
}

// Interface for student
export interface Student {
  name: string;
  isGovernmentForm: boolean;
  grades: number[]; 
  course: number;
  scholarship?: number;
  averageGrade(): number; 
}

// Interface for subject
export interface Subject {
  name: string;
  classroom: string;
  moduleCount: number;
}

// Interface for teacher
export interface Teacher {
  name: string;
  salaryRate: number;
  subjects: UniversitySubject[];
  workingDays: WeekDay[];
  PHD: boolean;
}