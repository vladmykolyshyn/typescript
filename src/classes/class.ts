import { WeekDay } from "../enums/enums";
import { Canteen, Faculty, Library, Section, Student, Subject, Teacher } from "../interfaces/interface";
import { RectorDecorator, logGetterSetter, OpportunityInfo, logTicketInfo, validatePHD } from "../decorators/decorator";
import { Application, OpenDayVisitor, Announcement, MyStudent, StudentIdName, StudentTicket } from "../types/type";

// Class for canteen
export class UniversityCanteen implements Canteen {
  constructor(
    public day: WeekDay,
    public menu: [string, number, string],
    public price: number
  ) {}
}

// Class for faculty
export class UniversityFaculty implements Faculty {
  constructor(
    public name: string,
    public dean: string,
    public governmentSeats: number
  ) {}
}

// Class for library
export class UniversityLibrary implements Library {
  constructor(
    public day: WeekDay,
    public openingTime: string,
    public closingTime: string
  ) {}
}

// Class for rector
@RectorDecorator
export class Rector {
  private signedApplications: Application[] = [];
  private openDayVisitors: OpenDayVisitor[] = [];

  greet(name: string): void {
    console.log(`Вітаю, ${name}!`);
  }

  signApplication(application: Application): void {
    this.signedApplications.push(application);
    console.log(`Заява на премію підписана: ${application.name}`);
  }

  makeAnnouncement(announcement: Announcement): void {
    console.log(`Оголошення: ${announcement.title}`);
    console.log(`Опис: ${announcement.description}`);
  }

  conductOpenDay(visitor: OpenDayVisitor): void {
    this.openDayVisitors.push(visitor);
    console.log(`Події: Ласкаво просимо на день відкритих дверей, ${visitor.name}!`);
  }
}

// Create class, which presents university with sports sections

export class University {
  private _sections: Section[];

  constructor() {
    this._sections = [];
  }

  // getter and setter for list of sections
  @logGetterSetter('Sections')
  get sections(): Section[] {
    return this._sections;
  }

  set sections(sections: Section[]) {
    this._sections = sections;
  }

  // Method for enrolling a student in a section
  enrollStudent(student: MyStudent, sectionId: number): void {
    const section = this.getSectionById(sectionId);
    if (section) {
      if (section.studentsSection.length < section.capacity) {
        section.studentsSection.push(student);
        console.log(`Студента ${student.name} успішно записано на секцію ${section.name}.`);
      } else {
        console.log(`Секція ${section.name} вже має максимальну кількість студентів.`);
      }
    } else {
      console.log(`Секцію з ID ${sectionId} не знайдено.`);
    }
  }

  // Method for withdrawing a student in a section
  withdrawStudent(student: MyStudent, sectionId: number): void {
    const section = this.getSectionById(sectionId);
    if (section) {
      const studentName = section.studentsSection.findIndex((s) => s.name === student.name);
      if (studentName > -1) {
        section.studentsSection.splice(studentName, 1);
        console.log(`Студента ${student.name} виписано з секції ${section.name}.`);
      } else {
        console.log(`Студент ${student.name} не знаходиться на секції ${section.name}.`);
      }
    } else {
      console.log(`Секцію з ID ${sectionId} не знайдено.`);
    }
  }

  // Private methof for getting section by ID
  private getSectionById(sectionId: number): Section | undefined {
    return this._sections.find((section) => section.id === sectionId);
  }
}

// Abstarc class, which represents an internship or practice opportunity
export abstract class InternshipOpportunity<T extends StudentIdName> {
  @OpportunityInfo
  abstract duration: number;

  constructor(public company: string) {}

  abstract isEligible(student: T): boolean;
}

// Class, which represents an internship opportunity
export class Internship<T extends StudentIdName> extends InternshipOpportunity<T> {
  @OpportunityInfo
  duration = 3; // during internship in monthes

  isEligible(student: T): boolean {
    // Using operator 'as' for type conversion 'unknown' to 'Student'
    const qualifiedStudent = student as StudentIdName;

    // Using the 'in' operator to check for the presence of the 'course' property on an object
    return 'course' in qualifiedStudent && qualifiedStudent.course >= 2;
  }
}

// Class, which represents a practice opportunity
export class Practice<T extends StudentIdName> extends InternshipOpportunity<T> {
  @OpportunityInfo
  duration = 2; // during internship in monthes

  isEligible(student: T): boolean {
    const qualifiedStudent = student as StudentIdName;
    return 'course' in qualifiedStudent && qualifiedStudent.course >= 2;
  }
}

// A class for managing the internship and internship process
export class InternshipManager<T extends StudentIdName> {
  private opportunities: InternshipOpportunity<T>[] = [];

  addOpportunity(opportunity: InternshipOpportunity<T>) {
    this.opportunities.push(opportunity);
  }

  placeStudents(students: T[]) {
    for (const student of students) {
      for (const opportunity of this.opportunities) {
        if (opportunity.isEligible(student)) {
          let type = '';
          if (opportunity instanceof Internship) {
            type = 'стажування';
          } else if (opportunity instanceof Practice) {
            type = 'практику';
          }
          console.log(`Студент ${student.name} буде проходити ${type} у ${opportunity.company}`);
        }
      }
    }
  }
}

// Class for student
export class UniversityStudent implements Student {
  constructor(
    public name: string,
    public isGovernmentForm: boolean,
    public grades: [number, number, number],
    public course: number,
    public scholarship?: number
  ) {}

  // Method for calculating the average grade
  averageGrade(): number {
    const sum = this.grades.reduce((total, grade) => total + grade, 0);
    const average = sum / this.grades.length;

    return Number(average.toFixed(1));
  }
}

// An abstract class that defines the general functionality for working with a student ticket
export abstract class StudentTicketHandler<T extends StudentTicket> {
  public tickets: T[] = [];

  // Adding a student card
  addTicket(ticket: T) {
    this.tickets.push(ticket);
  }

  // deletion of student ID card
  removeTicket(id: number) {
    this.tickets = this.tickets.filter((ticket) => ticket.id !== id);
  }

  // Getting the number of student tickets
  getTicketCount() {
    return this.tickets.length;
  }

  // Abstract class for ticket info
  abstract getTicketInfo(ticket: T): string;
}

// A class that specializes in working with student tickets of a specific faculty
export class FacultyStudentTicketHandler extends StudentTicketHandler<StudentTicket> {
  constructor(
    private faculty: string
  ) {
    super();
  }

  @logTicketInfo
  getTicketInfo(ticket: StudentTicket): string {
    return `Студент: ${ticket.name}\nФакультет: ${ticket.faculty}\nКурс: ${ticket.course}`;
  }
}

// Class for subject
export class UniversitySubject implements Subject {
  constructor(
    public name: string,
    public classroom: string,
    public moduleCount: number
  ) {}
}

// Class for teacher
export class UniversityTeacher implements Teacher {
  constructor(
    public name: string,
    public salaryRate: number,
    public subjects: UniversitySubject[],
    public workingDays: WeekDay[],
    public PHD: boolean
  ) {}

  @validatePHD
  attendSeminar(seminar: string) {
    console.log(`${this.name} відвідуватиме семінар: ${seminar}`);
  }

  // Method for calculating teacher's salary
  calculateSalary(): number { 
    return this.salaryRate * this.subjects.length;
  }
}