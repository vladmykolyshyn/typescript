// Type of documents
export type Announcement = {
  title: string;
  description: string;
};

export type Documenting = Announcement;

export type Application = {
  name: string;
  department: string;
} & Announcement;

export type OpenDayVisitor = {
  name: string;
  email: string;
};

// Type Guard for checking type of document
export function isApplication(doc: Documenting): doc is Application {
  return 'department' in doc;
}

// Allocate data types
export type MyStudent = Pick<StudentTicket, 'name' | 'course'>;

export type StudentIdName = Omit<StudentTicket, 'id' | 'faculty'>;

export type StudentTicket = {
  id: number;
  name: string;
  faculty: string;
  course: number;
};

 