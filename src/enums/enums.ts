export enum WeekDay {
  Monday = 'Monday',
  Tuesday = 'Tuesday',
  Wednesday = 'Wednesday',
  Thursday = 'Thursday',
  Friday = 'Friday',
  Saturday = 'Saturday',
  Sunday = 'Sunday'
}

export enum ScheduleforSport {
  Monday = 'every Monday after the last lesson',
  Tuesday = 'every Tuesday after the last lesson',
  Wednesday = 'every Wednesday after the last lesson',
  Thursday = 'every Thursday after the last lesson',
  Friday = 'every Frida after the last lesson'
}