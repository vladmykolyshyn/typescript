import { StudentTicket } from "../types/type";
import { Teacher } from "../interfaces/interface";

// Decorator for class Rector
export function RectorDecorator<T extends { new (...args: any[]): {} }>(constructor: T) {
  return class extends constructor {
    universityName: string = 'університу, у якому я є ректором';

    welcome(): void {
      console.log(`Ласкаво просимо до ${this.universityName}!`);
    }
  };
}

// Decorator for getter and setter
export function logGetterSetter(propertyName: string) {
  return function (target: any, propertyKey: string, descriptor: PropertyDescriptor) {
    const originalGet = descriptor.get;
    const originalSet = descriptor.set;

    descriptor.get = function () {
      console.log(`Отримано доступ до властивості ${propertyName}.`);
      return originalGet!.apply(this);
    };

    descriptor.set = function (value: any) {
      console.log(`Встановлено значення властивості ${propertyName}: ${JSON.stringify(value)}.`);
      originalSet!.apply(this, [value]);
    };
  };
}

// Decorator for class properties that stores information about internship and internship opportunities
export function OpportunityInfo(target: any, propertyKey: string) {
  const type = target.constructor;
  console.log(`Властивість ${propertyKey} має тип ${type?.name}`);
}

// A decorator that adds a login to the student card information method
export function logTicketInfo(target: any, key: string, descriptor: PropertyDescriptor) {
  const originalMethod = descriptor.value;

  descriptor.value = function (ticket: StudentTicket) {
    console.log(`Беремо інфомацію про студента: ${ticket.name}`);
    return originalMethod.apply(this, arguments);
  };

  return descriptor;
}

// Decorator for the class argument
export function validatePHD(target: any, propertyKey: string, descriptor: PropertyDescriptor) {
  const originalMethod = descriptor.value;

  descriptor.value = function (this: Teacher, ...args: any[]) {
    const teacher = this; 
    if (teacher.PHD) {
      return originalMethod.apply(this, args);
    } else {
      console.log("Teacher must have a PHD to attend the seminar.");
    }
  };

  return descriptor;
}
